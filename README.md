# Employee-method-in-Java



public class EmployeeMain3 {
   public static void main(String[] args){
   Employee edna = new Employee();
   Lawyer lucy = new Lawyer();
   Secretary stan = new Secretary();
   LegalSecretary leo = new LegalSecretary();
   
   printInfo(edna);
   printInfo(lucy);
   printInfo(stan);
   printInfo(leo);
 }
 
 // Prints information about sny kind of employee.
 public static void printf(Employee e) {
   System.out.print(e.gethours() + ", ");
   System.out.printf("$#.2f, ", e.getSalary());
   System.out.print(e.getVacationDays() + ", ");
   System.out.println(e); // toString representation of employee  
  }
}  
